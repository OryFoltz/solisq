
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2018 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f3xx_hal.h"

/* USER CODE BEGIN Includes */
#include <stdio.h>
#include <string.h>

/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/
SPI_HandleTypeDef hspi1;

UART_HandleTypeDef huart2;

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
const int MAX_DATA = 512;

char * tx_data = "Hello world o7...";
char rx_buffer[MAX_DATA] = {'\0'};

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_SPI1_Init(void);
static void MX_NVIC_Init(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart);

void Start_Command(void);

void FD_Reset(void);
uint8_t	FD_Set_Mode(void);
uint8_t FD_Check_Connection(void);
uint8_t FD_Mount(void);
void FD_Set_File(void);
uint8_t FD_Create(void);
uint8_t FD_Close(void);
uint8_t FD_Open(void);
uint8_t FD_Set_Pointer(uint8_t pointer);
uint8_t FD_Write(uint8_t * data);


uint8_t Create_File(void);
uint8_t Write_File(uint8_t * data_string);

uint8_t LCD_Setup(void);
uint8_t LCD_Clean_Buffer(void);
uint8_t LCD_Write_Data(char * string, uint8_t numof_chars, char column_address[2], char page_address);
void CHAR_Fetch(char index, char * return_string);

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  *
  * @retval None
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
   HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART2_UART_Init();
  MX_SPI1_Init();

  /* Initialize interrupts */
  MX_NVIC_Init();
  /* USER CODE BEGIN 2 */
	
	//__HAL_UART_ENABLE_IT(&huart2, UART_IT_RXNE);
	
	//Hold State
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_9, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_11, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_13, GPIO_PIN_RESET);


	//Set CS, A0 and Reset HIGH for SPI interfacing with LCD
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_9, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_11, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_13, GPIO_PIN_SET);
	HAL_Delay(100);
	
	uint8_t result = 0;
	char command;
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
	result = LCD_Setup();
	result = LCD_Clean_Buffer();
	
  while (1)
  {
  /* USER CODE END WHILE */
		HAL_Delay(1);
  /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */

}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART2;
  PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/**
  * @brief NVIC Configuration.
  * @retval None
  */
static void MX_NVIC_Init(void)
{
  /* RCC_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(RCC_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(RCC_IRQn);
  /* USART2_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(USART2_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(USART2_IRQn);
  /* SPI1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SPI1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(SPI1_IRQn);
}

/* SPI1 init function */
static void MX_SPI1_Init(void)
{

  /* SPI1 parameter configuration*/
  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_MASTER;
  hspi1.Init.Direction = SPI_DIRECTION_1LINE;
  hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi1.Init.NSS = SPI_NSS_SOFT;
  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_8;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 7;
  hspi1.Init.CRCLength = SPI_CRC_LENGTH_DATASIZE;
  hspi1.Init.NSSPMode = SPI_NSS_PULSE_DISABLE;
  if (HAL_SPI_Init(&hspi1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* USART2 init function */
static void MX_USART2_UART_Init(void)
{

  huart2.Instance = USART2;
  huart2.Init.BaudRate = 9600;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/** Configure pins as 
        * Analog 
        * Input 
        * Output
        * EVENT_OUT
        * EXTI
*/
static void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_9|GPIO_PIN_11|GPIO_PIN_13, GPIO_PIN_RESET);

  /*Configure GPIO pins : PD9 PD11 PD13 */
  GPIO_InitStruct.Pin = GPIO_PIN_9|GPIO_PIN_11|GPIO_PIN_13;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

//The response after a RX interupt completion
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	UNUSED(huart);
	
	HAL_UART_Receive_IT(&huart2, (uint8_t *)rx_buffer, 1);
}

//This function initializes a send command sequence to the ch376s
void Start_Command()
{	
	//Send two commands to initialize a command start to the ch376S
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)"57", 2);
	HAL_Delay(5);
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)"0AB", 3);
	HAL_Delay(5);
}

//This function will reset the ch376s
void FD_Reset()
{
	Start_Command();
	
	//Send reset command
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)"05", 2);
	
	//Allow time for reset
	HAL_Delay(100);
}

//This function will set the functionality of the USB connection. A 1 retuned means it 
//was succesful, a 0 means it failed, and a 2 means no USB connected
uint8_t FD_Set_Mode()
{
	uint8_t buffer_1[3]	= {'\0'};
	uint8_t buffer_2[3] = {'\0'};
	uint8_t return_bit	= 0;
	
	Start_Command();
	
	//Send Set Mode command, then send mode for USB-Host, producing SOF packages 
	//Note: see ch376s data sheet for CMD_SET_USB_MODE for more details
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)"15", 2);
	HAL_Delay(5);
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)"06", 2);
	HAL_Delay(5);
	
	//Wait and receive first two bytes from the ch376s (0x51 returned if command accepted)
	HAL_UART_Receive(&huart2, buffer_1, 2, 5000);
	
	//If command was accepted
	if ((buffer_1[0] == '1') && (buffer_1[1] == '5'))
	{
		HAL_UART_Receive(&huart2, buffer_2, 2, HAL_MAX_DELAY);
		
		//Check if USB is connected (0x15 will be returned if USB is connected)
		if ((buffer_2[0] == '5') && (buffer_2[1] == '1'))
			return_bit = 1;
		
		//Else, no USB connection
		else
			return_bit = 2;
	}
	
	return return_bit;
}

//This function will check to see if a connection exists between the STM32 and the ch376s.
//If connection exists a 1 will be returned, else a 0
uint8_t FD_Check_Connection()
{
	uint8_t buffer[3] 	= {'\0'};
	uint8_t return_bit	= 0;
	
	Start_Command();
	
	//Send command, then identifier
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)0x06, 2);
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)0x00, 2);
	
	//Receive answer
	HAL_UART_Receive(&huart2, buffer, 2, HAL_MAX_DELAY);
	
	//If it returned the inverse of what was sent, connection still established (0xFF returned)
	if ((buffer[0] == 'F') && (buffer[1] == 'F'))
		return_bit = 1;
	
	return return_bit;
}

//This function will mount the flashdrive to the USB reader
uint8_t FD_Mount()
{
	uint8_t buffer[3] 	= {'\0'};
	uint8_t return_bit 	= 0;
	
	Start_Command();
	
	//Send command to mount memory drive
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)0x31, 2);
	
	//Receive answer
	HAL_UART_Receive(&huart2, buffer, 2, HAL_MAX_DELAY);
	
	//If the mount was succesful (0x14 returned)
	if ((buffer[0] == '4') && (buffer[1] == '1'))
		return_bit = 1;
	
	return return_bit;
}

//This function will set the filename for file creation
void FD_Set_File()
{
	Start_Command();
	
	//Initialize filename write, requires double clarification
	//The file name will then be sent, followed by sending 0x00 to end name transmission
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)0x2F, 2);
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)0x2F, 2);
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)"DATA.TXT", 2);
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)0x00, 2);
	
	//Allow time to set file name
	HAL_Delay(20);
}

//This function will write data to the file
uint8_t FD_Write(uint8_t * data)
{
	uint8_t return_bit = 0;
	uint8_t buffer[3] = {'\0'};
	uint8_t size = sizeof(data);
	
	
	Start_Command();
	
	//Send command to start data write, telling how many bytes to expect
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)0x3C, 2);
	HAL_UART_Transmit_IT(&huart2, &size, 1);
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)0x00, 2);
	
	//Receive reply from ch376s, if 0x1E is read, device in write mode
	HAL_UART_Receive(&huart2, buffer, 2, HAL_MAX_DELAY);
		
	//If reply receive was 0x1E
	if ((buffer[0] == 'E') && (buffer[1] == '1'))
	{
		Start_Command();
	
		//Send command to Write_File data to storage, then write data
		HAL_UART_Transmit_IT(&huart2, (uint8_t *)0x2D, 2);
		HAL_UART_Transmit_IT(&huart2, data, size);
		
		//Receive reply from ch376s
		HAL_UART_Receive(&huart2, buffer, 2, HAL_MAX_DELAY);
		
		//If write accepted
		if (((buffer[0] == '4') && (buffer[1] == '1')) || ((buffer[0] == 'F') && (buffer[1] == 'F')))
			return_bit = 1;
	}
	
	//Update file size
	Start_Command();
	
	//Write command to update file size
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)0x3D, 2);
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)0x0, 1);
	
	//Receive answer
	HAL_UART_Receive(&huart2, buffer, 2, HAL_MAX_DELAY);
	
	//If update did not complete (!0x14)
	if ((buffer[0] != '4') && (buffer[1] != '1'))
		return_bit = 0;
	
	return return_bit;
}

//This function will create the file
uint8_t FD_Create()
{
	uint8_t return_bit = 0;
	uint8_t buffer[3] = {'\0'};
	
	Start_Command();
	
	//Transmit command to create file based on the name set by FD_Set_File()
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)0x34, 2);
	
	//Receive reply
	HAL_UART_Receive(&huart2, buffer, 2, HAL_MAX_DELAY);
	
	//If completed, USB device will return 0x14
	if ((buffer[0] == '4') && (buffer[1] == '1'))
		return_bit = 0;
	
	return return_bit;
}

//This will close the file
uint8_t FD_Close()
{
	uint8_t return_bit = 0;
	uint8_t buffer[3] = {'\0'};
	
	Start_Command();
	
	//Send command to close file, then tell to update file
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)0x36, 2);
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)0x01, 2);
	
	//Receive answer from ch376s
	HAL_UART_Receive(&huart2, buffer, 2, HAL_MAX_DELAY);
	
	//If file closed (0x14 returned)
	if ((buffer[0] == '4') && (buffer[1] == '1'))
		return_bit = 1;
	
	return return_bit;		
}

//This will open the file
uint8_t FD_Open()
{
	uint8_t return_bit 	= 0;
	uint8_t buffer[3] 	= {'\0'};
	
	Start_Command();
	
	//Send command to open file
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)0x32, 2);
	
	//Receive answer
	HAL_UART_Receive(&huart2, buffer, 2, HAL_MAX_DELAY);
	
	//If command was completed, 0x14 will be received
	if ((buffer[0] == '4') && (buffer[1] == '1'))
		return_bit = 1;
	
	return return_bit;
}

//This function will set the file pointer based on the input
uint8_t FD_Set_Pointer(uint8_t pointer)
{
	uint8_t return_bit 	= 0;
	uint8_t buffer[3] 	= {'\0'};
	
	Start_Command();
	
	//Transmit command to set pointer
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)0x39, 2);
	
	//If set to beginning, pointer == 0
	if (!pointer)
	{
		HAL_UART_Transmit_IT(&huart2, (uint8_t *)0x00, 2);
		HAL_UART_Transmit_IT(&huart2, (uint8_t *)0x00, 2);
		HAL_UART_Transmit_IT(&huart2, (uint8_t *)0x00, 2);
		HAL_UART_Transmit_IT(&huart2, (uint8_t *)0x00, 2);
	}
	
	//Else set to end of file
	else
	{
		HAL_UART_Transmit_IT(&huart2, (uint8_t *)0xFF, 2);
		HAL_UART_Transmit_IT(&huart2, (uint8_t *)0xFF, 2);
		HAL_UART_Transmit_IT(&huart2, (uint8_t *)0xFF, 2);
		HAL_UART_Transmit_IT(&huart2, (uint8_t *)0xFF, 2);
	}
	
	HAL_UART_Receive(&huart2, buffer, 2, HAL_MAX_DELAY);
	
	//If command was accepted (0x14)
	if ((buffer[0] == '4') && (buffer[1] == '1'))
		return_bit = 1;
	
	return return_bit;
}

//This function will create a file 
uint8_t Create_File()
{
	uint8_t return_bit 	= 0;
	uint8_t catch_bit		= 0;
	
	//Reset Device
	FD_Reset();
	
	//Set mode
	catch_bit = FD_Set_Mode();
	
	//Determine success of set mode
	switch (catch_bit)
	{
		case 0:
			return_bit = 0;
			break;
			
		case 1:
			return_bit = 1;
			break;
		
		case 2:
			return_bit = 0;
			break;
		
		default:
			return_bit = 0;
			break;
	}
	
	//If success, check connection with device
	if (return_bit)
		return_bit = FD_Check_Connection();
	
	//If success, mount device
	if (return_bit)
		return_bit = FD_Mount();
	
	//If success, set file name for creation
	if (return_bit)
		FD_Set_File();
	
	//Create file
	return_bit = FD_Create();
	
	//If file was created succesfully 
	if (return_bit)
		return_bit = FD_Close();
	
	return return_bit;
}

//This function will write data to the file
uint8_t Write_File(uint8_t * data)
{
	uint8_t return_bit 	= 0;
	uint8_t catch_bit		= 0;
	
	//Reset Device
	FD_Reset();
	
	//Set mode
	catch_bit = FD_Set_Mode();
	
	//Determine success of set mode
	switch (catch_bit)
	{
		case 0:
			return_bit = 0;
			break;
			
		case 1:
			return_bit = 1;
			break;
		
		case 2:
			return_bit = 0;
			break;
		
		default:
			return_bit = 0;
			break;
	}
	
	//If success, check connection with device
	if (return_bit)
		return_bit = FD_Check_Connection();
	
	//If success, mount device
	if (return_bit)
		return_bit = FD_Mount();
	
	//If success, set file name for creation
	if (return_bit)
		FD_Set_File();
	
	//If success, open file
	if (return_bit)
		return_bit = FD_Open();
	
	//If success, set file pointer to end of file
	if (return_bit)
		return_bit = FD_Set_Pointer(0);
	
	//If success, begin writing data
	if (return_bit)
		return_bit = FD_Write(data);
	
	//If data was written correctly
	if (return_bit)
		return_bit = FD_Close();
	
	return return_bit;
}

//This function will setup the LCD screen and its settings
uint8_t LCD_Setup()
{
	//Pin PD9 is CS
	//Pin PD11 is A0
	//Pin PD13 is reset
	
	HAL_StatusTypeDef result;
	char command = {'\0'};
	
	//Reset device hardware
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_13, GPIO_PIN_RESET);
	HAL_Delay(1);
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_13, GPIO_PIN_SET);
	
	//Set A0 low to indicate incoming commands
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_11, GPIO_PIN_RESET);
	
	
	//Ensure SPI intitialized
	HAL_SPI_Transmit_IT(&hspi1, (uint8_t *)0x0, 1);
	HAL_Delay(1);

	//Send command to set ram to normal correspondence to segment driver output, set cs low
	command = 0xA0;
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_9, GPIO_PIN_RESET);
	result = HAL_SPI_Transmit_IT(&hspi1, (uint8_t *)&command, 1);
	while (hspi1.State ==  HAL_SPI_STATE_BUSY_TX);
	
	//If not sent correctly, cs high, report fail
	if (result != HAL_OK)
	{
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_9, GPIO_PIN_SET);
		return 0;
	}
	
	//Else send command to turn display off 
	else
	{
		command = 0xAE;
		result = HAL_SPI_Transmit_IT(&hspi1, (uint8_t *)&command, 1);
		while (hspi1.State ==  HAL_SPI_STATE_BUSY_TX);
	}
	
	//If not sent correctly, CS high, report fail
	if (result != HAL_OK)
	{
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_9, GPIO_PIN_SET);
		return 0;
	}
	
	//Else send command to set scan direction of output terminal
	else 
	{
		command = 0xC8;
		result = HAL_SPI_Transmit_IT(&hspi1, (uint8_t *)&command, 1);
		while (hspi1.State ==  HAL_SPI_STATE_BUSY_TX);
	}
	
	//If not sent correctly, CS high report fail
	if (result != HAL_OK)
	{
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_9, GPIO_PIN_SET);
		return 0;
	}
	
	//Else send command to set LCD bias ratio for voltage
	else
	{
		command = 0xA2;
		result = HAL_SPI_Transmit_IT(&hspi1, (uint8_t *)&command, 1);
		while (hspi1.State ==  HAL_SPI_STATE_BUSY_TX);
	}
	
	//If not sent correctly, CS high, report fail
	if (result != HAL_OK)
	{
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_9, GPIO_PIN_SET);
		return 0;
	}
	
	//Else send command to set the power supply circuits
	else
	{
		command = 0x2F;
		result = HAL_SPI_Transmit_IT(&hspi1, (uint8_t *)&command, 1);
		while (hspi1.State ==  HAL_SPI_STATE_BUSY_TX);
	}
	
	//If not sent correctly, CS high, report fail
	if (result != HAL_OK)
	{
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_9, GPIO_PIN_SET);
		return 0;
	}
	
	//Else send command to set the internal resistor ratio
	else
	{
		command = 0x21;
		result = HAL_SPI_Transmit_IT(&hspi1, (uint8_t *)&command, 1);
		while (hspi1.State ==  HAL_SPI_STATE_BUSY_TX);
	}
	
	//If not sent correctly, CS high, report fail
	if (result != HAL_OK)
	{
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_9, GPIO_PIN_SET);
		return 0;
	}
	
	//Else send command to set the electronic volume mode
	else
	{
		command = 0x81;
		result = HAL_SPI_Transmit_IT(&hspi1, (uint8_t *)&command, 1);
		while (hspi1.State ==  HAL_SPI_STATE_BUSY_TX);
	}
	
	//If not sent correctly, CS high, report fail
	if (result != HAL_OK)
	{
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_9, GPIO_PIN_SET);
		return 0;
	}
	
	//Else send the command to set the electronic volume register
	else
	{
		command = 0x1F;
		result = HAL_SPI_Transmit_IT(&hspi1, (uint8_t *)&command, 1);
		while (hspi1.State ==  HAL_SPI_STATE_BUSY_TX);
	}
	
	//If not sent correctly, CS high,report fail
	if (result != HAL_OK)
	{
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_9, GPIO_PIN_SET);
		return 0;
	}
	
	//Else turn the screen on
	else
	{
		command = 0xAF;
		result = HAL_SPI_Transmit_IT(&hspi1, (uint8_t *)&command, 1);
		while (hspi1.State ==  HAL_SPI_STATE_BUSY_TX);
	}
	
	//If not sent correctly, CS high report fail
	if (result != HAL_OK)
	{
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_9, GPIO_PIN_SET);
		return 0;
	}
	
	//Else return to top of screen
	else
	{
		command = 0x40;
		result = HAL_SPI_Transmit_IT(&hspi1, (uint8_t *)&command, 1);
		while (hspi1.State ==  HAL_SPI_STATE_BUSY_TX);
	}
	
	//If not sent correctly, CS high, report fail
	if (result != HAL_OK)
	{
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_9, GPIO_PIN_SET);
		return 0;
	}
	
	//All transmissions successful, CS high, report complete
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_9, GPIO_PIN_SET);
	return 1;		
}

//This function will clean the buffer so the display shows a blank screen
uint8_t LCD_Clean_Buffer()
{
	char LCD_buffer[128] = {'\0'};
	HAL_StatusTypeDef result;
	char command;
	
		// Set the page cursor to page 0, set A0 low for command, CS low, write command, wait for TX finish
		command = 0xB0;
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_11, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_9, GPIO_PIN_RESET);
		result = HAL_SPI_Transmit_IT(&hspi1, (uint8_t *)&command, 1);
		while (hspi1.State ==  HAL_SPI_STATE_BUSY_TX);
	
		//If transmission fail, CS high, return fail
		if (result != HAL_OK)
		{
			HAL_GPIO_WritePin(GPIOD, GPIO_PIN_9, GPIO_PIN_SET);
			return 0;
		}
	
		//Else set AO to 1 for data transmission, wait for tx complete
		else
		{
			HAL_GPIO_WritePin(GPIOD, GPIO_PIN_11, GPIO_PIN_SET);
			result = HAL_SPI_Transmit_IT(&hspi1, (uint8_t *)LCD_buffer, 128);
			while (hspi1.State ==  HAL_SPI_STATE_BUSY_TX);
		}
		
		//If transmission fail, CS high, return fail
		if (result != HAL_OK)
		{
			HAL_GPIO_WritePin(GPIOD, GPIO_PIN_9, GPIO_PIN_SET);
			return 0;
		}
	
		//Else, command to move column cursor lower address bits, set AO to 0 for incoming command, send command, wait for transmission
		else 
		{
			command = 0x00;
			HAL_GPIO_WritePin(GPIOD, GPIO_PIN_11, GPIO_PIN_RESET);
			result = HAL_SPI_Transmit_IT(&hspi1, (uint8_t *)&command, 1);
			while (hspi1.State ==  HAL_SPI_STATE_BUSY_TX);
		}
		
		//If transmission fail, CS high, return fail
		if (result != HAL_OK)
		{
			HAL_GPIO_WritePin(GPIOD, GPIO_PIN_9, GPIO_PIN_SET);
			return 0;
		}
	
		//Else send command for upper column bit cursor positionm, send command, wait for tranmission
		else
		{
			command = 0x10;
			result = HAL_SPI_Transmit_IT(&hspi1, (uint8_t *)&command, 1);
			while (hspi1.State ==  HAL_SPI_STATE_BUSY_TX);
		}
		
		//If transmission fail, CS high, return fail
		if (result != HAL_OK)
		{
			HAL_GPIO_WritePin(GPIOD, GPIO_PIN_9, GPIO_PIN_SET);
			return 0;
		}
		
		//Else send command to move page cursor to next page, wait for transmission
		else
		{
			command = 0xB1;
			result = HAL_SPI_Transmit_IT(&hspi1, (uint8_t *)&command, 1);
			while (hspi1.State ==  HAL_SPI_STATE_BUSY_TX);
		}
		
		//If transmission fail, CS high, return fail
		if (result != HAL_OK)
		{
			HAL_GPIO_WritePin(GPIOD, GPIO_PIN_9, GPIO_PIN_SET);
			return 0;
		}
		
		//Else set A0 to 1 for incoming data, send data, wait for transmission
		else
		{
			HAL_GPIO_WritePin(GPIOD, GPIO_PIN_11, GPIO_PIN_SET);
			result = HAL_SPI_Transmit_IT(&hspi1, (uint8_t *)LCD_buffer, 128);
			while (hspi1.State ==  HAL_SPI_STATE_BUSY_TX);
		}
		
		
		//If transmission fail, CS high, return fail
		if (result != HAL_OK)
		{
			HAL_GPIO_WritePin(GPIOD, GPIO_PIN_9, GPIO_PIN_SET);
			return 0;
		}
		
		//Else set A0 to 0 for command, send command to move column cursor, wait for transmission
		else
		{
			command = 0x00;
			HAL_GPIO_WritePin(GPIOD, GPIO_PIN_11, GPIO_PIN_RESET);
			result = HAL_SPI_Transmit_IT(&hspi1, (uint8_t *)&command, 1);
			while (hspi1.State ==  HAL_SPI_STATE_BUSY_TX);
		}
	
		//If transmisison fail, CS high, return fail
		if (result != HAL_OK)
		{
			HAL_GPIO_WritePin(GPIOD, GPIO_PIN_9, GPIO_PIN_SET);
			return 0;
		}
		
		//Send command to move column cursor, wait for transmission
		else
		{
			command = 0x10;
			result = HAL_SPI_Transmit_IT(&hspi1, (uint8_t *)&command, 1);
			while (hspi1.State ==  HAL_SPI_STATE_BUSY_TX);
		}
		
		//If transmission fail
		if (result != HAL_OK)
		{
			HAL_GPIO_WritePin(GPIOD, GPIO_PIN_9, GPIO_PIN_SET);
			return 0;
		}
		
		//Else send command to move to next page, wait for transmission
		else
		{
			command = 0xB2;
			result = HAL_SPI_Transmit_IT(&hspi1, (uint8_t *)&command, 1);
			while (hspi1.State ==  HAL_SPI_STATE_BUSY_TX);
		}
		
		//If transmission fail, CS high, return fail
		if (result != HAL_OK)
		{
			HAL_GPIO_WritePin(GPIOD, GPIO_PIN_9, GPIO_PIN_SET);
			return 0;
		}
		
		//Else set A0 to 1 for incoming data, send data, wait for transmission
		else
		{
			HAL_GPIO_WritePin(GPIOD, GPIO_PIN_11, GPIO_PIN_SET);
			result = HAL_SPI_Transmit_IT(&hspi1, (uint8_t *)LCD_buffer, 128);
			while (hspi1.State ==  HAL_SPI_STATE_BUSY_TX);
		}
		
		//If transmisison fail, CS high, return fail
		if (result != HAL_OK)
		{
			HAL_GPIO_WritePin(GPIOD, GPIO_PIN_9, GPIO_PIN_SET);
			return 0;
		}
		
		//Else set A0 to 0 for incoming command, send command to adjust colunm cursor, wait for transmission
		else
		{
			command = 0x00;
			HAL_GPIO_WritePin(GPIOD, GPIO_PIN_11, GPIO_PIN_RESET);
			result = HAL_SPI_Transmit_IT(&hspi1, (uint8_t *)&command, 1);
			while (hspi1.State ==  HAL_SPI_STATE_BUSY_TX);
		}
	
		//If transmission fail, CS high, return fail
		if (result != HAL_OK)
		{
			HAL_GPIO_WritePin(GPIOD, GPIO_PIN_9, GPIO_PIN_SET);
			return 0;
		}
		
		//Else send command to adjust column cursor, wait for transmission
		else
		{
			command = 0x10;
			result = HAL_SPI_Transmit_IT(&hspi1, (uint8_t *)&command, 1);
			while (hspi1.State ==  HAL_SPI_STATE_BUSY_TX);
		}
		
		//If transmission fail, CS high, return fail
		if (result != HAL_OK)
		{
			HAL_GPIO_WritePin(GPIOD, GPIO_PIN_9, GPIO_PIN_SET);
			return 0;
		}
		
		//Else send command to move to next page, wait for transmission
		else
		{
			command = 0xB3;
			result = HAL_SPI_Transmit_IT(&hspi1, (uint8_t *)&command, 1);
			while (hspi1.State ==  HAL_SPI_STATE_BUSY_TX);
		}
		
		//If transmission fails, CS high, return fail
		if (result != HAL_OK)
		{
			HAL_GPIO_WritePin(GPIOD, GPIO_PIN_9, GPIO_PIN_SET);
			return 0;
		}
		
		//Else set A0 to 1 for incoming data, send data, wait for transmission
		else
		{
			HAL_GPIO_WritePin(GPIOD, GPIO_PIN_11, GPIO_PIN_SET);
			result = HAL_SPI_Transmit_IT(&hspi1, (uint8_t *)LCD_buffer, 128);
			while (hspi1.State ==  HAL_SPI_STATE_BUSY_TX);
		}

		//If transmission fails, CS high, return fail
		if (result != HAL_OK)
		{
			HAL_GPIO_WritePin(GPIOD, GPIO_PIN_9, GPIO_PIN_SET);
			return 0;
		}
		
		//Else set A0 to 0 for incoming command, send command to adjust colunm cursor, wait for transmission
		else
		{
			command = 0x00;
			HAL_GPIO_WritePin(GPIOD, GPIO_PIN_11, GPIO_PIN_RESET);
			result = HAL_SPI_Transmit_IT(&hspi1, (uint8_t *)&command, 1);
			while (hspi1.State ==  HAL_SPI_STATE_BUSY_TX);
		}
	
		//If transmission fail, CS high, return fail
		if (result != HAL_OK)
		{
			HAL_GPIO_WritePin(GPIOD, GPIO_PIN_9, GPIO_PIN_SET);
			return 0;
		}
		
		//Else send command to adjust column cursor, wait for transmission
		else
		{
			command = 0x10;
			result = HAL_SPI_Transmit_IT(&hspi1, (uint8_t *)&command, 1);
			while (hspi1.State ==  HAL_SPI_STATE_BUSY_TX);
		}
		
		//If transmission fail, CS high, return fail
		if (result != HAL_OK)
		{
			HAL_GPIO_WritePin(GPIOD, GPIO_PIN_9, GPIO_PIN_SET);
			return 0;
		}
		
		//Else send command to move to first page, wait for transmission
		else
		{
			command = 0xB0;
			result = HAL_SPI_Transmit_IT(&hspi1, (uint8_t *)&command, 1);
			while (hspi1.State ==  HAL_SPI_STATE_BUSY_TX);
		}
		
		//Transmissions complete, CS high
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_9, GPIO_PIN_SET);
		
		//If transmission fail, return fail
		
		if (result != HAL_OK)
			return 0;
		
		//Else transmissions were successful, return success
		else
			return 1;
}

uint8_t LCD_Write_Data(char * data_string, uint8_t numof_chars, char column_address[2], char page_address)
{
	HAL_StatusTypeDef result;
	
	//Set A0 to send command, CS low
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_11, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_9, GPIO_PIN_RESET);
	
	//Send command for first address set
	result = HAL_SPI_Transmit_IT(&hspi1, (uint8_t *)&column_address[0], 128);
	while (hspi1.State ==  HAL_SPI_STATE_BUSY_TX);
	
	//If transmission failed, CS high, end and submit fail
	if (result != HAL_OK)
	{
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_9, GPIO_PIN_SET);
		return 0;
	}
	
	//Else send command for second column address set
	else
	{
		result = HAL_SPI_Transmit_IT(&hspi1, (uint8_t *)&column_address[1], 128);
		while (hspi1.State ==  HAL_SPI_STATE_BUSY_TX);
	}
	
	//If transmission failed, CS high, end and submit fail
	if (result != HAL_OK)
	{
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_9, GPIO_PIN_SET);
		return 0;	
	}
	
	//Else send command for page address
	else
	{
		result = HAL_SPI_Transmit_IT(&hspi1, (uint8_t *)&page_address, 128);
		while (hspi1.State ==  HAL_SPI_STATE_BUSY_TX);
	}
	
	//If transmission failed, CS high, end and submit fail
	if (result != HAL_OK)
	{
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_9, GPIO_PIN_SET);
		return 0;	
	}
	
	//Else set A0 to 1 for data, and send data
	else
	{
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_11, GPIO_PIN_RESET);
		result = HAL_SPI_Transmit_IT(&hspi1, (uint8_t *)data_string, numof_chars);
		while (hspi1.State ==  HAL_SPI_STATE_BUSY_TX);
	}
	
	//CS high, ending transmissions
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_9, GPIO_PIN_SET);
	
	//If transmission failed, end and submit fail
	if (result != HAL_OK)
		return 0;	
	
	//Transmissions successful
	return 1;
}

//This function fetches the hex code for each individual char sent to it and returns it
void CHAR_Fetch(char index, char * return_string)
{		
	char index_char[6] = {'\0'};
	
	//Select based on index, then set index_char to corresponding data and return
	switch (index)
	{
		case 'A':
			index_char[0] = 0x78;
			index_char[1] = 0x16;
			index_char[2] = 0x11;
			index_char[3] = 0x16;
			index_char[4] = 0x78;
			index_char[5] = 0x00;
			break;
		
		case 'B':
			index_char[0] = 0xFF;
			index_char[1] = 0x49;
			index_char[2] = 0x49;
			index_char[3] = 0x49;
			index_char[4] = 0x36;
			index_char[5] = 0x00;
			break;
		
		case 'C':
			index_char[0] = 0x3E;
			index_char[1] = 0x41;
			index_char[2] = 0x41;
			index_char[3] = 0x41;
			index_char[4] = 0x41;
			index_char[5] = 0x00;
			break;
		
		case 'D':		
			index_char[0] = 0x7F;
			index_char[1] = 0x41;
			index_char[2] = 0x41;
			index_char[3] = 0x22;
			index_char[4] = 0x1C;
			index_char[5] = 0x00;
			break;
		
		case 'E':
			index_char[0] = 0x7F;
			index_char[1] = 0x49;
			index_char[2] = 0x49;
			index_char[3] = 0x49;
			index_char[4] = 0x49;
			index_char[5] = 0x00;
			break;
		
		case 'F':
			index_char[0] = 0x7F;
			index_char[1] = 0x09;
			index_char[2] = 0x09;
			index_char[3] = 0x09;
			index_char[4] = 0x01;
			index_char[5] = 0x00;
			break;
		
		case 'G':
			index_char[0] = 0x7F;
			index_char[1] = 0x41;
			index_char[2] = 0x49;
			index_char[3] = 0x79;
			index_char[4] = 0x08;
			index_char[5] = 0x00;
			break;
		
		case 'H':
			index_char[0] = 0x7F;
			index_char[1] = 0x08;
			index_char[2] = 0x08;
			index_char[3] = 0x08;
			index_char[4] = 0x7F;
			index_char[5] = 0x00;
			break;
		
		case 'I':
			index_char[0] = 0x41;
			index_char[1] = 0x41;
			index_char[2] = 0x7F;
			index_char[3] = 0x41;
			index_char[4] = 0x41;
			index_char[5] = 0x00;
			break;
		
		case 'J':
			index_char[0] = 0x71;
			index_char[1] = 0x41;
			index_char[2] = 0x7F;
			index_char[3] = 0x01;
			index_char[4] = 0x01;
			index_char[5] = 0x00;
			break;
		
		case 'K':
			index_char[0] = 0x7F;
			index_char[1] = 0x08;
			index_char[2] = 0x14;
			index_char[3] = 0x22;
			index_char[4] = 0x41;
			index_char[5] = 0x00;
			break;
		
		case 'L':
			index_char[0] = 0x7F;
			index_char[1] = 0x40;
			index_char[2] = 0x40;
			index_char[3] = 0x40;
			index_char[4] = 0x40;
			index_char[5] = 0x00;
			break;
		
		case 'M':
			index_char[0] = 0x7F;
			index_char[1] = 0x01;
			index_char[2] = 0x0F;
			index_char[3] = 0x01;
			index_char[4] = 0x7F;
			index_char[5] = 0x00;
			break;
		
		case 'N':
			index_char[0] = 0x7F;
			index_char[1] = 0x02;
			index_char[2] = 0x1C;
			index_char[3] = 0x20;
			index_char[4] = 0x7F;
			index_char[5] = 0x00;
			break;
		
		case 'O':
			index_char[0] = 0x3E;
			index_char[1] = 0x41;
			index_char[2] = 0x41;
			index_char[3] = 0x41;
			index_char[4] = 0x3E;
			index_char[5] = 0x00;
			break;
		
		case 'P':
			index_char[0] = 0x7F;
			index_char[1] = 0x09;
			index_char[2] = 0x09;
			index_char[3] = 0x09;
			index_char[4] = 0x06;
			index_char[5] = 0x00;
			break;
		
		case 'Q':
			index_char[0] = 0x3E;
			index_char[1] = 0x41;
			index_char[2] = 0x51;
			index_char[3] = 0x21;
			index_char[4] = 0x5E;
			index_char[5] = 0x00;
			break;
		
		case 'R':
			index_char[0] = 0x7F;
			index_char[1] = 0x09;
			index_char[2] = 0x09;
			index_char[3] = 0x09;
			index_char[4] = 0x66;
			index_char[5] = 0x00;
			break;
		
		case 'S':
			index_char[0] = 0x46;
			index_char[1] = 0x49;
			index_char[2] = 0x49;
			index_char[3] = 0x49;
			index_char[4] = 0x31;
			index_char[5] = 0x00;
			break;
		
		case 'T':
			index_char[0] = 0x01;
			index_char[1] = 0x01;
			index_char[2] = 0x7F;
			index_char[3] = 0x01;
			index_char[4] = 0x01;
			index_char[5] = 0x00;
			break;
		
		case 'U':
			index_char[0] = 0x3F;
			index_char[1] = 0x40;
			index_char[2] = 0x40;
			index_char[3] = 0x40;
			index_char[4] = 0x3F;
			index_char[5] = 0x00;
			break;
		
		case 'V':
			index_char[0] = 0x1F;
			index_char[1] = 0x20;
			index_char[2] = 0x40;
			index_char[3] = 0x20;
			index_char[4] = 0x1F;
			index_char[5] = 0x00;
			break;
		
		case 'W':
			index_char[0] = 0x3F;
			index_char[1] = 0x40;
			index_char[2] = 0x78;
			index_char[3] = 0x40;
			index_char[4] = 0x3F;
			index_char[5] = 0x00;
			break;
		
		case 'X':
			index_char[0] = 0x63;
			index_char[1] = 0x14;
			index_char[2] = 0x08;
			index_char[3] = 0x14;
			index_char[4] = 0x63;
			index_char[5] = 0x00;
			break;
		
		case 'Y':
			index_char[0] = 0x03;
			index_char[1] = 0x0C;
			index_char[2] = 0x70;
			index_char[3] = 0x0C;
			index_char[4] = 0x03;
			index_char[5] = 0x00;
			break;
		
		case 'Z':
			index_char[0] = 0x61;
			index_char[1] = 0x51;
			index_char[2] = 0x49;
			index_char[3] = 0x45;
			index_char[4] = 0x43;
			index_char[5] = 0x00;
			break;
		
		case '0':
			index_char[0] = 0x7F;
			index_char[1] = 0x43;
			index_char[2] = 0x5D;
			index_char[3] = 0x61;
			index_char[4] = 0x7F;
			index_char[5] = 0x00;
			break;
		
		case '1':
			index_char[0] = 0x41;
			index_char[1] = 0x41;
			index_char[2] = 0x7F;
			index_char[3] = 0x40;
			index_char[4] = 0x40;
			index_char[5] = 0x00;
			break;
		
		case '2':
			index_char[0] = 0x31;
			index_char[1] = 0x49;
			index_char[2] = 0x49;
			index_char[3] = 0x49;
			index_char[4] = 0x46;
			index_char[5] = 0x00;
			break;
		
		case '3':
			index_char[0] = 0x41;
			index_char[1] = 0x49;
			index_char[2] = 0x49;
			index_char[3] = 0x49;
			index_char[4] = 0x36;
			index_char[5] = 0x00;
			break;
			
		case '4':
			index_char[0] = 0x0F;
			index_char[1] = 0x08;
			index_char[2] = 0x08;
			index_char[3] = 0x7F;
			index_char[4] = 0x08;
			index_char[5] = 0x00;
			break;
		
			case '5':
			index_char[0] = 0x4F;
			index_char[1] = 0x49;
			index_char[2] = 0x49;
			index_char[3] = 0x49;
			index_char[4] = 0x31;
			index_char[5] = 0x00;
			break;
			
		case '6':
			index_char[0] = 0x3E;
			index_char[1] = 0x49;
			index_char[2] = 0x49;
			index_char[3] = 0x49;
			index_char[4] = 0x31;
			index_char[5] = 0x00;
			break;
		
		case '7':
			index_char[0] = 0x01;
			index_char[1] = 0x01;
			index_char[2] = 0x01;
			index_char[3] = 0x01;
			index_char[4] = 0x7F;
			index_char[5] = 0x00;
			break;
		
		case '8':
			index_char[0] = 0x36;
			index_char[1] = 0x49;
			index_char[2] = 0x49;
			index_char[3] = 0x49;
			index_char[4] = 0x36;
			index_char[5] = 0x00;
			break;
		
		case '9':
			index_char[0] = 0x06;
			index_char[1] = 0x09;
			index_char[2] = 0x09;
			index_char[3] = 0x09;
			index_char[4] = 0x7E;
			index_char[5] = 0x00;
			break;
		
		case '.':
			index_char[0] = 0x60;
			index_char[1] = 0x60;
			index_char[2] = 0x00;
			index_char[3] = 0x00;
			index_char[4] = 0x00;
			index_char[5] = 0x00;
			break;
			
		case ':':
			index_char[0] = 0x00;
			index_char[1] = 0x00;
			index_char[2] = 0x36;
			index_char[3] = 0x00;
			index_char[4] = 0x00;
			index_char[5] = 0x00;
			break;
			
		case '/':
			index_char[0] = 0x60;
			index_char[1] = 0x10;
			index_char[2] = 0x08;
			index_char[3] = 0x04;
			index_char[4] = 0x03;
			index_char[5] = 0x00;
			break;
			
		case ' ':
			index_char[0] = 0x00;
			index_char[1] = 0x00;
			index_char[2] = 0x00;
			index_char[3] = 0x00;
			index_char[4] = 0x00;
			index_char[5] = 0x00;
			break;
			
		//If a character is not recognized
		default:
			index_char[0] = 0x00;
			index_char[1] = 0x3E;
			index_char[2] = 0x3E;
			index_char[3] = 0x3E;
			index_char[4] = 0x00;
			index_char[5] = 0x00;
			break;
	}
	
	//set string to index_char so that it may be modified from calling function
	return_string = index_char;
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  file: The file name as string.
  * @param  line: The line in file as a number.
  * @retval None
  */
void _Error_Handler(char *file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
