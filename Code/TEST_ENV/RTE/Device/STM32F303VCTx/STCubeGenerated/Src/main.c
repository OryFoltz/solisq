
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2018 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f3xx_hal.h"

/* USER CODE BEGIN Includes */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/
SPI_HandleTypeDef hspi1;
UART_HandleTypeDef huart2;

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
const int MAX_DATA = 512;
char rx_buffer[MAX_DATA] = {'\0'};

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_SPI1_Init(void);
static void MX_NVIC_Init(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
void Start_Command(void);

void FD_Reset(void);
uint8_t	FD_Set_Mode(void);
uint8_t FD_Check_Connection(void);
uint8_t FD_Mount(void);
void FD_Set_File(uint8_t * file_name);
uint8_t FD_Create(void);
uint8_t FD_Close(void);
uint8_t FD_Open(void);
uint8_t FD_Set_Pointer(uint8_t pointer);
uint8_t FD_Write(uint8_t data[]);

uint8_t Create_File(uint8_t * file_name);
uint8_t Write_File(uint8_t * data_string, uint8_t * file_name);

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart);

uint8_t LCD_Setup(void);
uint8_t LCD_Clean_Buffer(void);
uint8_t LCD_Write_Data(char string[], uint8_t numof_chars, char column_addr_1, char column_addr_2, char page_address);
void CHAR_Fetch(char index, char data_string[]);

uint8_t Display_Menu(void);
uint8_t Display_Velocity(void);

char ConvertHexToChar(char hex_char);
void CharToLCDValue(char array[], uint8_t buffer_size, uint32_t value);
void ConvertStringToLCD(char data_string[], char array[]);

uint8_t Debounce(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin);
uint8_t ButtonPress(uint8_t s_timer);
uint8_t CheckButtons(void);

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  *
  * @retval None
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART2_UART_Init();
  MX_SPI1_Init();

  /* Initialize interrupts */
  MX_NVIC_Init();
  /* USER CODE BEGIN 2 */
	
	//__HAL_UART_ENABLE_IT(&huart2, UART_IT_RXNE);
	
	//Hold State
	HAL_GPIO_WritePin(GPIOD, SPI_CS_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOD, LCD_COMMAND_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOD, LCD_RESET_Pin, GPIO_PIN_RESET);


	//Set CS, A0 and Reset HIGH for SPI interfacing with LCD
	HAL_GPIO_WritePin(SPI_CS_GPIO_Port, SPI_CS_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(LCD_COMMAND_GPIO_Port, LCD_COMMAND_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(LCD_RESET_GPIO_Port, LCD_RESET_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(RGB_RED_GPIO_Port, RGB_RED_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(RGB_GREEN_GPIO_Port, RGB_GREEN_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(RGB_BLUE_GPIO_Port, RGB_BLUE_Pin, GPIO_PIN_SET);
	HAL_Delay(100);
	
	uint8_t result 				= 0;
	uint8_t button_press 	= 0;
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
	result = LCD_Setup();
	result = LCD_Clean_Buffer();
	
  while (1)
  {
  /* USER CODE END WHILE */		

  /* USER CODE BEGIN 3 */
		Display_Menu();
		
		while(1)
		{
			button_press = CheckButtons();
			
			switch (button_press)
			{
				case 0:
					Display_Velocity();
					Display_Menu();
				
					//Make sure button 0 isnt pressed
					while(HAL_GPIO_ReadPin(BUTTON_0_GPIO_Port, BUTTON_0_Pin) == GPIO_PIN_RESET);
					break;
				
				default:
					break;
			}
			
			HAL_Delay(500);
		}		
  }
  /* USER CODE END 3 */

}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART2;
  PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/**
  * @brief NVIC Configuration.
  * @retval None
  */
static void MX_NVIC_Init(void)
{
  /* RCC_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(RCC_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(RCC_IRQn);
  /* USART2_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(USART2_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(USART2_IRQn);
  /* SPI1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SPI1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(SPI1_IRQn);
}

/* SPI1 init function */
static void MX_SPI1_Init(void)
{

  /* SPI1 parameter configuration*/
  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_MASTER;
  hspi1.Init.Direction = SPI_DIRECTION_1LINE;
  hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi1.Init.NSS = SPI_NSS_SOFT;
  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_8;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 7;
  hspi1.Init.CRCLength = SPI_CRC_LENGTH_DATASIZE;
  hspi1.Init.NSSPMode = SPI_NSS_PULSE_DISABLE;
  if (HAL_SPI_Init(&hspi1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* USART2 init function */
static void MX_USART2_UART_Init(void)
{

  huart2.Instance = USART2;
  huart2.Init.BaudRate = 9600;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/** Configure pins as 
        * Analog 
        * Input 
        * Output
        * EVENT_OUT
        * EXTI
*/
static void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOD, SPI_CS_Pin|LCD_COMMAND_Pin|LCD_RESET_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(RGB_BLUE_GPIO_Port, RGB_BLUE_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, RGB_GREEN_Pin|RGB_RED_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : SPI_CS_Pin LCD_COMMAND_Pin LCD_RESET_Pin */
  GPIO_InitStruct.Pin = SPI_CS_Pin|LCD_COMMAND_Pin|LCD_RESET_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pins : PROPELLOR_INPUT_Pin BUTTON_5_Pin BUTTON_4_Pin BUTTON_3_Pin */
  GPIO_InitStruct.Pin = PROPELLOR_INPUT_Pin|BUTTON_5_Pin|BUTTON_4_Pin|BUTTON_3_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pin : RGB_BLUE_Pin */
  GPIO_InitStruct.Pin = RGB_BLUE_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(RGB_BLUE_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : RGB_GREEN_Pin RGB_RED_Pin */
  GPIO_InitStruct.Pin = RGB_GREEN_Pin|RGB_RED_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : BUTTON_2_Pin BUTTON_1_Pin BUTTON_0_Pin */
  GPIO_InitStruct.Pin = BUTTON_2_Pin|BUTTON_1_Pin|BUTTON_0_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */
//This function initializes a send command sequence to the ch376s
void Start_Command()
{	
	char SC_1 = 0x57;
	char SC_2 = 0xAB;
	
	//Send two commands to initialize a command start to the ch376S
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)&SC_1, 1);
	while(huart2.gState != HAL_UART_STATE_READY);
	HAL_Delay(1);
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)&SC_2, 1);
	while(huart2.gState != HAL_UART_STATE_READY);
	HAL_Delay(1);
}

//This function will reset the ch376s
void FD_Reset()
{
	HAL_Delay(50);
	
	char reset_command = 0x05;
	
	Start_Command();
	
	//Send reset command
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)&reset_command, 1);
	
	//Allow time for reset
	HAL_Delay(100);
}

//This function will set the functionality of the USB connection. A 1 retuned means it 
//was succesful, a 0 means it failed, and a 2 means no USB connected
uint8_t FD_Set_Mode()
{
	HAL_Delay(50);
	char usb_c_1 = 0x15;
	char usb_c_2 = 0x06;
	uint8_t buffer[2] = {'\0'};
	uint8_t return_bit	= 0;
	
	Start_Command();
	
	//Send Set Mode command, then send mode for USB-Host, producing SOF packages 
	//Note: see ch376s data sheet for CMD_SET_USB_MODE for more details
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)&usb_c_1, 1);
	while(huart2.gState != HAL_UART_STATE_READY);
	HAL_Delay(1);
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)&usb_c_2, 1);
	while(huart2.gState != HAL_UART_STATE_READY);
	
	//Wait and receive first two bytes from the ch376s (0x51 returned if command accepted)
	HAL_UART_Receive(&huart2, buffer, 2, HAL_MAX_DELAY);
	
	//If command was accepted
	if (buffer[0] == 0x51)
	{		
		//Check if USB is connected (0x15 will be returned if USB is connected)
		if (buffer[1] == 0x15)
			return_bit = 1;
		
		//Else, no USB connection
		else
			return_bit = 2;
	}
	
	return return_bit;
}

//This function will check to see if a connection exists between the STM32 and the ch376s.
//If connection exists a 1 will be returned, else a 0
uint8_t FD_Check_Connection()
{
	HAL_Delay(50);
	uint8_t buffer 			= 0;
	uint8_t return_bit	= 0;
	char connection_1_c = 0x06;
	char connection_2_c = 0x00;
	
	Start_Command();
	
	//Send command, then identifier
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)&connection_1_c, 1);
	while(huart2.gState != HAL_UART_STATE_READY);
	HAL_Delay(1);
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)&connection_2_c, 1);
	while(huart2.gState != HAL_UART_STATE_READY);
	
	//Receive answer
	HAL_UART_Receive(&huart2, &buffer, 1, HAL_MAX_DELAY);
	
	//If it returned the inverse of what was sent, connection still established (0xFF returned)
	if (buffer == 0xFF)
		return_bit = 1;
	
	return return_bit;
}

//This function will mount the flashdrive to the USB reader
uint8_t FD_Mount()
{
	HAL_Delay(50);
	uint8_t buffer 	= 0;
	uint8_t return_bit 	= 0;
	char mount_command 	= 0x31;
	
	Start_Command();
	
	//Send command to mount memory drive
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)&mount_command, 1);
	while(huart2.gState != HAL_UART_STATE_READY);
	
	//Receive answer
	HAL_UART_Receive(&huart2, &buffer, 1, HAL_MAX_DELAY);
	
	//If the mount was succesful (0x14 returned)
	if (buffer == 0x14)
		return_bit = 1;
	
	return return_bit;
}

//This function will set the filename for file creation
void FD_Set_File(uint8_t * file_name)
{
	HAL_Delay(50);
	char sf_c_1 	= 0x2F;
	char sf_c_2 	= 0x00;
	uint8_t count = 0;
	uint8_t size 	= (sizeof(*file_name) * (strlen((char *)file_name)));
	uint8_t file_name_buffer[132] = {'\0'};
	
	for (count = 0; count < size; count++)
		file_name_buffer[count] = file_name[count];
	
	Start_Command();
	
	//Initialize filename write, requires double clarification
	//The file name will then be sent, followed by sending 0x00 to end name transmission
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)&sf_c_1, 1);
	while(huart2.gState != HAL_UART_STATE_READY);
	HAL_Delay(1);
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)&sf_c_1, 1);
	while(huart2.gState != HAL_UART_STATE_READY);
	HAL_Delay(1);
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)file_name_buffer, size);
	while(huart2.gState != HAL_UART_STATE_READY);
	HAL_Delay(1);
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)&sf_c_2, 1);
	while(huart2.gState != HAL_UART_STATE_READY);
	HAL_Delay(1);
	
	//Allow time to set file name
	HAL_Delay(20);
}

//This function will write data to the file
uint8_t FD_Write(uint8_t data[])
{
	HAL_Delay(50);
	uint8_t return_bit 				= 0;
	uint8_t buffer 						= 0;
	uint8_t data_buffer[516] 	= {'\0'};
	uint8_t size 							= (sizeof(*data) * strlen((char *) data));
	uint8_t count 						= 0;
	char write_c_1 = 0x3C;
	char write_c_2 = 0x00;
	char write_c_3 = 0x2D;
	char write_c_4 = 0x3D;
	char write_c_5 = 0x00;
	
	for (count = 0; count < size; count++)
		data_buffer[count] = data[count];
	
	Start_Command();
	
	//Send command to start data write, telling how many bytes to expect
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)&write_c_1, 1);
	while(huart2.gState != HAL_UART_STATE_READY);
	HAL_Delay(1);
	HAL_UART_Transmit_IT(&huart2, &size, 1);
	while(huart2.gState != HAL_UART_STATE_READY);
	HAL_Delay(1);
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)&write_c_2, 1);
	while(huart2.gState != HAL_UART_STATE_READY);
	
	//Receive reply from ch376s, if 0x1E is read, device in write mode
	HAL_UART_Receive(&huart2, &buffer, 1, HAL_MAX_DELAY);
		
	//If reply receive was 0x1E
	if (buffer == 0x1E)
	{
		Start_Command();
	
		//Send command to Write_File data to storage, then write data
		HAL_UART_Transmit_IT(&huart2, (uint8_t *)&write_c_3, 1);
		while(huart2.gState != HAL_UART_STATE_READY);
		HAL_Delay(1);
		HAL_UART_Transmit_IT(&huart2, data_buffer, size);
		while(huart2.gState != HAL_UART_STATE_READY);
		
		//Receive reply from ch376s
		HAL_UART_Receive(&huart2, &buffer, 1, HAL_MAX_DELAY);
		
		//If write accepted
		if (buffer != 0x00)
			return_bit = 1;
	}
	
	//Update file size
	Start_Command();
	
	//Write command to update file size
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)&write_c_4, 1);
	while(huart2.gState != HAL_UART_STATE_READY);
	HAL_Delay(1);
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)&write_c_5, 1);
	while(huart2.gState != HAL_UART_STATE_READY);
	
	//Receive answer
	HAL_UART_Receive(&huart2, &buffer, 1, HAL_MAX_DELAY);
	
	//If update did not complete (!0x14)
	if (buffer != 0x14)
		return_bit = 0;
	
	return return_bit;
}

//This function will create the file
uint8_t FD_Create()
{
	HAL_Delay(50);
	uint8_t return_bit 	= 0;
	uint8_t buffer			= 0;
	char create_command = 0x34;
	
	Start_Command();
	
	//Transmit command to create file based on the name set by FD_Set_File()
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)&create_command, 1);
	while(huart2.gState != HAL_UART_STATE_READY);
	
	//Receive reply
	HAL_UART_Receive(&huart2, &buffer, 1, HAL_MAX_DELAY);
	
	//If completed, USB device will return 0x14
	if (buffer == 0x14)
		return_bit = 1;
	
	return return_bit;
}

//This will close the file
uint8_t FD_Close()
{
	HAL_Delay(50);
	uint8_t return_bit 	= 0;
	uint8_t buffer			= 0;
	char close_c_1 = 0x36;
	char close_c_2 = 0x01;
	
	Start_Command();
	
	//Send command to close file, then tell to update file
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)&close_c_1, 1);
	while(huart2.gState != HAL_UART_STATE_READY);
	HAL_Delay(1);
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)&close_c_2, 1);
	while(huart2.gState != HAL_UART_STATE_READY);
	
	//Receive answer from ch376s
	HAL_UART_Receive(&huart2, &buffer, 1, HAL_MAX_DELAY);
	
	//If file closed (0x14 returned)
	if (buffer == 0x14)
		return_bit = 1;
	
	//Wait fo rfile to close
	HAL_Delay(50);
	
	return return_bit;		
}

//This will open the file
uint8_t FD_Open()
{
	HAL_Delay(50);
	uint8_t return_bit 	= 0;
	uint8_t buffer 			= 0;
	char open_command 	= 0x32;
	
	Start_Command();
	
	//Send command to open file
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)&open_command, 1);
	while(huart2.gState != HAL_UART_STATE_READY);
	
	//Receive answer
	HAL_UART_Receive(&huart2, &buffer, 1, HAL_MAX_DELAY);
	
	//If command was completed, 0x14 will be received
	if (buffer == 0x14)
		return_bit = 1;
	
	//Wait for file to open
	HAL_Delay(50);
	
	return return_bit;
}

//This function will set the file pointer based on the input
uint8_t FD_Set_Pointer(uint8_t pointer)
{
	HAL_Delay(50);
	uint8_t return_bit 	= 0;
	uint8_t buffer			= 0;
	char fd_c_1 				= 0x39;
	char fd_c_2 				= 0x00;
	char fd_c_3 				= 0xFF;
	
	Start_Command();
	
	//Transmit command to set pointer
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)&fd_c_1, 1);
	while(huart2.gState != HAL_UART_STATE_READY);
	HAL_Delay(1);
	
	//If set to beginning, pointer == 0
	if (!pointer)
	{
		HAL_UART_Transmit_IT(&huart2, (uint8_t *)&fd_c_2, 1);
		while(huart2.gState != HAL_UART_STATE_READY);
		HAL_Delay(1);
		HAL_UART_Transmit_IT(&huart2, (uint8_t *)&fd_c_2, 1);
		while(huart2.gState != HAL_UART_STATE_READY);
		HAL_Delay(1);
		HAL_UART_Transmit_IT(&huart2, (uint8_t *)&fd_c_2, 1);
		while(huart2.gState != HAL_UART_STATE_READY);
		HAL_Delay(1);
		HAL_UART_Transmit_IT(&huart2, (uint8_t *)&fd_c_2, 1);
		while(huart2.gState != HAL_UART_STATE_READY);
		HAL_Delay(1);
	}
	
	//Else set to end of file
	else
	{
		HAL_UART_Transmit_IT(&huart2, (uint8_t *)&fd_c_3, 1);
		while(huart2.gState != HAL_UART_STATE_READY);
		HAL_Delay(1);
		HAL_UART_Transmit_IT(&huart2, (uint8_t *)&fd_c_3, 1);
		while(huart2.gState != HAL_UART_STATE_READY);
		HAL_Delay(1);
		HAL_UART_Transmit_IT(&huart2, (uint8_t *)&fd_c_3, 1);
		while(huart2.gState != HAL_UART_STATE_READY);
		HAL_Delay(1);
		HAL_UART_Transmit_IT(&huart2, (uint8_t *)&fd_c_3, 1);
		while(huart2.gState != HAL_UART_STATE_READY);
	}
	
	HAL_UART_Receive(&huart2, &buffer, 1, HAL_MAX_DELAY);
	
	//If command was accepted (0x14)
	if (buffer == 0x14)
		return_bit = 1;
	
	return return_bit;
}

//This function will create a file 
uint8_t Create_File(uint8_t * file_name)
{
	uint8_t return_bit 	= 0;
	uint8_t catch_bit		= 0;
	
	//Reset Device
	FD_Reset();
	
	//Set mode
	catch_bit = FD_Set_Mode();
	
	//Determine success of set mode
	switch (catch_bit)
	{
		case 0:
			return_bit = 0;
			break;
			
		case 1:
			return_bit = 1;
			break;
		
		case 2:
			return_bit = 0;
			break;
		
		default:
			return_bit = 0;
			break;
	}
	
	//If success, check connection with device
	if (return_bit)
		return_bit = FD_Check_Connection();
	
	//If success, mount device
	if (return_bit)
		return_bit = FD_Mount();
	
	//If success, set file name for creation
	if (return_bit)
		FD_Set_File(file_name);
	
	//Create file
	if (return_bit)
		return_bit = FD_Create();
	
	//If file was created succesfully 
	if (return_bit)
		return_bit = FD_Close();
	
	return return_bit;
}

//This function will write data to the file
uint8_t Write_File(uint8_t * data, uint8_t * file_name)
{
	uint8_t return_bit 	= 0;
	uint8_t catch_bit		= 0;
	
	//Reset Device
	FD_Reset();
	
	//Set mode
	catch_bit = FD_Set_Mode();
	
	//Determine success of set mode
	switch (catch_bit)
	{
		case 0:
			return_bit = 0;
			break;
			
		case 1:
			return_bit = 1;
			break;
		
		case 2:
			return_bit = 0;
			break;
		
		default:
			return_bit = 0;
			break;
	}
	
	//If success, check connection with device
	if (return_bit)
		return_bit = FD_Check_Connection();
	
	//If success, mount device
	if (return_bit)
		return_bit = FD_Mount();
	
	//If success, set file name for creation
	if (return_bit)
		FD_Set_File(file_name);
	
	//If success, open file
	if (return_bit)
		return_bit = FD_Open();
	
	//If success, set file pointer to end of file
	if (return_bit)
		return_bit = FD_Set_Pointer(0);
	
	//If success, begin writing data
	if (return_bit)
		return_bit = FD_Write(data);
	
	//If data was written correctly
	if (return_bit)
		return_bit = FD_Close();
	
	return return_bit;
}

//The response after a RX interupt completion
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	UNUSED(huart);
	
	HAL_UART_Receive_IT(&huart2, (uint8_t *)rx_buffer, 1);
}

//This function will setup the LCD screen and its settings
uint8_t LCD_Setup()
{
	//Pin PD9 is CS
	//Pin PD11 is A0
	//Pin PD13 is reset
	
	HAL_StatusTypeDef result;
	char command = {'\0'};
	
	//Reset device hardware
	HAL_GPIO_WritePin(LCD_RESET_GPIO_Port, LCD_RESET_Pin, GPIO_PIN_RESET);
	HAL_Delay(2);
	HAL_GPIO_WritePin(LCD_RESET_GPIO_Port, LCD_RESET_Pin, GPIO_PIN_SET);
	
	//Set A0 low to indicate incoming commands
	HAL_GPIO_WritePin(LCD_COMMAND_GPIO_Port, LCD_COMMAND_Pin, GPIO_PIN_RESET);
	
	
	//Ensure SPI intitialized
	HAL_SPI_Transmit_IT(&hspi1, (uint8_t *)0x0, 1);
	HAL_Delay(1);

	//Send command to set ram to normal correspondence to segment driver output, set cs low
	command = 0xA2;
	HAL_GPIO_WritePin(SPI_CS_GPIO_Port, SPI_CS_Pin, GPIO_PIN_RESET);
	result = HAL_SPI_Transmit_IT(&hspi1, (uint8_t *)&command, 1);
	while (hspi1.State ==  HAL_SPI_STATE_BUSY_TX);
	
	//If not sent correctly, cs high, report fail
	if (result != HAL_OK)
	{
		HAL_GPIO_WritePin(SPI_CS_GPIO_Port, SPI_CS_Pin, GPIO_PIN_SET);
		return 0;
	}
	
	//Else send command to turn display off 
	else
	{
		command = 0xA0;
		result = HAL_SPI_Transmit_IT(&hspi1, (uint8_t *)&command, 1);
		while (hspi1.State ==  HAL_SPI_STATE_BUSY_TX);
	}
	
	//If not sent correctly, CS high, report fail
	if (result != HAL_OK)
	{
		HAL_GPIO_WritePin(SPI_CS_GPIO_Port, SPI_CS_Pin, GPIO_PIN_SET);
		return 0;
	}
	
	//Else send command to set scan direction of output terminal
	else 
	{
		command = 0xC8;
		result = HAL_SPI_Transmit_IT(&hspi1, (uint8_t *)&command, 1);
		while (hspi1.State ==  HAL_SPI_STATE_BUSY_TX);
	}
	
	//If not sent correctly, CS high report fail
	if (result != HAL_OK)
	{
		HAL_GPIO_WritePin(SPI_CS_GPIO_Port, SPI_CS_Pin, GPIO_PIN_SET);
		return 0;
	}
	
	//Else send command to set LCD bias ratio for voltage
	else
	{
		command = 0x23;
		result = HAL_SPI_Transmit_IT(&hspi1, (uint8_t *)&command, 1);
		while (hspi1.State ==  HAL_SPI_STATE_BUSY_TX);
	}
	
	//If not sent correctly, CS high, report fail
	if (result != HAL_OK)
	{
		HAL_GPIO_WritePin(SPI_CS_GPIO_Port, SPI_CS_Pin, GPIO_PIN_SET);
		return 0;
	}
	
	//Else send command to set the power supply circuits
	else
	{
		command = 0x2F;
		result = HAL_SPI_Transmit_IT(&hspi1, (uint8_t *)&command, 1);
		while (hspi1.State ==  HAL_SPI_STATE_BUSY_TX);
	}
	
	//If not sent correctly, CS high, report fail
	if (result != HAL_OK)
	{
		HAL_GPIO_WritePin(SPI_CS_GPIO_Port, SPI_CS_Pin, GPIO_PIN_SET);
		return 0;
	}
	
	//Else send command to set the internal resistor ratio
	else
	{
		command = 0x81;
		result = HAL_SPI_Transmit_IT(&hspi1, (uint8_t *)&command, 1);
		while (hspi1.State ==  HAL_SPI_STATE_BUSY_TX);
	}
	
	//If not sent correctly, CS high, report fail
	if (result != HAL_OK)
	{
		HAL_GPIO_WritePin(SPI_CS_GPIO_Port, SPI_CS_Pin, GPIO_PIN_SET);
		return 0;
	}
	
	//Else send command to set the electronic volume mode
	else
	{
		command = 0x2F;
		result = HAL_SPI_Transmit_IT(&hspi1, (uint8_t *)&command, 1);
		while (hspi1.State ==  HAL_SPI_STATE_BUSY_TX);
	}
	
	//If not sent correctly, CS high, report fail
	if (result != HAL_OK)
	{
		HAL_GPIO_WritePin(SPI_CS_GPIO_Port, SPI_CS_Pin, GPIO_PIN_SET);
		return 0;
	}
	
	//Else send the command to set the electronic volume register
	else
	{
		command = 0x40;
		result = HAL_SPI_Transmit_IT(&hspi1, (uint8_t *)&command, 1);
		while (hspi1.State ==  HAL_SPI_STATE_BUSY_TX);
	}
	
	//If not sent correctly, CS high,report fail
	if (result != HAL_OK)
	{
		HAL_GPIO_WritePin(SPI_CS_GPIO_Port, SPI_CS_Pin, GPIO_PIN_SET);
		return 0;
	}
	
	//Else turn the screen on
	else
	{
		command = 0xAF;
		result = HAL_SPI_Transmit_IT(&hspi1, (uint8_t *)&command, 1);
		while (hspi1.State ==  HAL_SPI_STATE_BUSY_TX);
	}
	
	//CS high
	HAL_GPIO_WritePin(SPI_CS_GPIO_Port, SPI_CS_Pin, GPIO_PIN_SET);
	
	//If report fail
	if (result != HAL_OK)
		return 0;
	
	//All transmissions successful, report complete
	return 1;		
}

//This function will clean the buffer so the display shows a blank screen
uint8_t LCD_Clean_Buffer()
{
	char LCD_buffer[129] = {'\0'};
	HAL_StatusTypeDef result;
	uint8_t count = 0;
	char command;
	char addr_1 = 0x10;
	char addr_2 = 0x00;
	
	//CS low, set command for first page
	HAL_GPIO_WritePin(SPI_CS_GPIO_Port, SPI_CS_Pin, GPIO_PIN_RESET);
	command = 0xB0;
	
	for (count = 0; count < 8; count++, command++)
	{
		// Set the page cursor to page 0, set A0 low for command, write command, wait for TX finish
		HAL_GPIO_WritePin(LCD_COMMAND_GPIO_Port, LCD_COMMAND_Pin, GPIO_PIN_RESET);
		result = HAL_SPI_Transmit_IT(&hspi1, (uint8_t *)&command, 1);
		while (hspi1.State ==  HAL_SPI_STATE_BUSY_TX);
		
		//If transmission fail, CS high, return fail
		if (result != HAL_OK)
		{
			HAL_GPIO_WritePin(SPI_CS_GPIO_Port, SPI_CS_Pin, GPIO_PIN_SET);
			return 0;
		}

		//Else, command to move column cursor lower address bits, set AO to 0 for incoming command, send command, wait for transmission
		else 
		{
			HAL_GPIO_WritePin(LCD_COMMAND_GPIO_Port, LCD_COMMAND_Pin, GPIO_PIN_RESET);
			result = HAL_SPI_Transmit_IT(&hspi1, (uint8_t *)&addr_1, 1);
			while (hspi1.State ==  HAL_SPI_STATE_BUSY_TX);
		}

		//If transmission fail, CS high, return fail
		if (result != HAL_OK)
		{
			HAL_GPIO_WritePin(SPI_CS_GPIO_Port, SPI_CS_Pin, GPIO_PIN_SET);
			return 0;
		}

		//Else send command for upper column bit cursor positionm, send command, wait for tranmission
		else
		{
			result = HAL_SPI_Transmit_IT(&hspi1, (uint8_t *)&addr_2, 1);
			while (hspi1.State ==  HAL_SPI_STATE_BUSY_TX);
		}

		//If transmission fail, CS high, return fail
		if (result != HAL_OK)
		{
			HAL_GPIO_WritePin(SPI_CS_GPIO_Port, SPI_CS_Pin, GPIO_PIN_SET);
			return 0;
		}

		//Else set AO to 1 for data transmission, wait for tx complete
		else
		{
			HAL_GPIO_WritePin(LCD_COMMAND_GPIO_Port, LCD_COMMAND_Pin, GPIO_PIN_SET);
			result = HAL_SPI_Transmit_IT(&hspi1, (uint8_t *)LCD_buffer, 129);
			while (hspi1.State ==  HAL_SPI_STATE_BUSY_TX);
		}
		
			//If transmission fail, CS high, return fail
		if (result != HAL_OK)
		{
			HAL_GPIO_WritePin(SPI_CS_GPIO_Port, SPI_CS_Pin, GPIO_PIN_SET);
			return 0;
		}		
	}
	//CS high, Return success
	//Transmissions complete, CS high
	HAL_GPIO_WritePin(SPI_CS_GPIO_Port, SPI_CS_Pin, GPIO_PIN_SET);
	return 1;
}

uint8_t LCD_Write_Data(char data_string[], uint8_t numof_chars, char column_addr_1, char column_addr_2, char page_address)
{
	HAL_StatusTypeDef result;
	
	//Set A0 to send command, CS low
	HAL_GPIO_WritePin(LCD_COMMAND_GPIO_Port, LCD_COMMAND_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(SPI_CS_GPIO_Port, SPI_CS_Pin, GPIO_PIN_RESET);
	
	//Send command for first address set
	result = HAL_SPI_Transmit_IT(&hspi1, (uint8_t *)&column_addr_1, 1);
	while (hspi1.State ==  HAL_SPI_STATE_BUSY_TX);
	
	//If transmission failed, CS high, end and submit fail
	if (result != HAL_OK)
	{
		HAL_GPIO_WritePin(SPI_CS_GPIO_Port, SPI_CS_Pin, GPIO_PIN_SET);
		return 0;
	}
	
	//Else send command for second column address set
	else
	{
		result = HAL_SPI_Transmit_IT(&hspi1, (uint8_t *)&column_addr_2, 1);
		while (hspi1.State ==  HAL_SPI_STATE_BUSY_TX);
	}
	
	//If transmission failed, CS high, end and submit fail
	if (result != HAL_OK)
	{
		HAL_GPIO_WritePin(SPI_CS_GPIO_Port, SPI_CS_Pin, GPIO_PIN_SET);
		return 0;	
	}
	
	//Else send command for page address
	else
	{
		result = HAL_SPI_Transmit_IT(&hspi1, (uint8_t *)&page_address, 1);
		while (hspi1.State ==  HAL_SPI_STATE_BUSY_TX);
	}
	
	//If transmission failed, CS high, end and submit fail
	if (result != HAL_OK)
	{
		HAL_GPIO_WritePin(SPI_CS_GPIO_Port, SPI_CS_Pin, GPIO_PIN_SET);
		return 0;	
	}
	
	//Else set A0 to 1 for data, and send data
	else
	{
		HAL_GPIO_WritePin(LCD_COMMAND_GPIO_Port, LCD_COMMAND_Pin, GPIO_PIN_SET);
		result = HAL_SPI_Transmit_IT(&hspi1, (uint8_t *)data_string, numof_chars);
		while (hspi1.State ==  HAL_SPI_STATE_BUSY_TX);
	}
	
	//CS high, ending transmissions
	HAL_GPIO_WritePin(SPI_CS_GPIO_Port, SPI_CS_Pin, GPIO_PIN_SET);
	
	//If transmission failed, end and submit fail
	if (result != HAL_OK)
		return 0;	
	
	//Transmissions successful
	return 1;
}

//This function fetches the hex code for each individual char sent to it and returns it
void CHAR_Fetch(char index, char data_string[])
{		
	char index_char[6] = {'\0'};
	
	//Select based on index, then set index_char to corresponding data and return
	switch (index)
	{
		case 'A':
			index_char[0] = 0x78;
			index_char[1] = 0x16;
			index_char[2] = 0x11;
			index_char[3] = 0x16;
			index_char[4] = 0x78;
			index_char[5] = 0x00;
			break;
		
		case 'B':
			index_char[0] = 0x7F;
			index_char[1] = 0x49;
			index_char[2] = 0x49;
			index_char[3] = 0x49;
			index_char[4] = 0x36;
			index_char[5] = 0x00;
			break;
		
		case 'C':
			index_char[0] = 0x3E;
			index_char[1] = 0x41;
			index_char[2] = 0x41;
			index_char[3] = 0x41;
			index_char[4] = 0x41;
			index_char[5] = 0x00;
			break;
		
		case 'D':		
			index_char[0] = 0x7F;
			index_char[1] = 0x41;
			index_char[2] = 0x41;
			index_char[3] = 0x22;
			index_char[4] = 0x1C;
			index_char[5] = 0x00;
			break;
		
		case 'E':
			index_char[0] = 0x7F;
			index_char[1] = 0x49;
			index_char[2] = 0x49;
			index_char[3] = 0x49;
			index_char[4] = 0x49;
			index_char[5] = 0x00;
			break;
		
		case 'F':
			index_char[0] = 0x7F;
			index_char[1] = 0x09;
			index_char[2] = 0x09;
			index_char[3] = 0x09;
			index_char[4] = 0x01;
			index_char[5] = 0x00;
			break;
		
		case 'G':
			index_char[0] = 0x7F;
			index_char[1] = 0x41;
			index_char[2] = 0x49;
			index_char[3] = 0x79;
			index_char[4] = 0x08;
			index_char[5] = 0x00;
			break;
		
		case 'H':
			index_char[0] = 0x7F;
			index_char[1] = 0x08;
			index_char[2] = 0x08;
			index_char[3] = 0x08;
			index_char[4] = 0x7F;
			index_char[5] = 0x00;
			break;
		
		case 'I':
			index_char[0] = 0x41;
			index_char[1] = 0x41;
			index_char[2] = 0x7F;
			index_char[3] = 0x41;
			index_char[4] = 0x41;
			index_char[5] = 0x00;
			break;
		
		case 'J':
			index_char[0] = 0x71;
			index_char[1] = 0x41;
			index_char[2] = 0x7F;
			index_char[3] = 0x01;
			index_char[4] = 0x01;
			index_char[5] = 0x00;
			break;
		
		case 'K':
			index_char[0] = 0x7F;
			index_char[1] = 0x08;
			index_char[2] = 0x14;
			index_char[3] = 0x22;
			index_char[4] = 0x41;
			index_char[5] = 0x00;
			break;
		
		case 'L':
			index_char[0] = 0x7F;
			index_char[1] = 0x40;
			index_char[2] = 0x40;
			index_char[3] = 0x40;
			index_char[4] = 0x40;
			index_char[5] = 0x00;
			break;
		
		case 'M':
			index_char[0] = 0x7F;
			index_char[1] = 0x01;
			index_char[2] = 0x0F;
			index_char[3] = 0x01;
			index_char[4] = 0x7F;
			index_char[5] = 0x00;
			break;
		
		case 'N':
			index_char[0] = 0x7F;
			index_char[1] = 0x02;
			index_char[2] = 0x1C;
			index_char[3] = 0x20;
			index_char[4] = 0x7F;
			index_char[5] = 0x00;
			break;
		
		case 'O':
			index_char[0] = 0x3E;
			index_char[1] = 0x41;
			index_char[2] = 0x41;
			index_char[3] = 0x41;
			index_char[4] = 0x3E;
			index_char[5] = 0x00;
			break;
		
		case 'P':
			index_char[0] = 0x7F;
			index_char[1] = 0x09;
			index_char[2] = 0x09;
			index_char[3] = 0x09;
			index_char[4] = 0x06;
			index_char[5] = 0x00;
			break;
		
		case 'Q':
			index_char[0] = 0x3E;
			index_char[1] = 0x41;
			index_char[2] = 0x51;
			index_char[3] = 0x21;
			index_char[4] = 0x5E;
			index_char[5] = 0x00;
			break;
		
		case 'R':
			index_char[0] = 0x7F;
			index_char[1] = 0x09;
			index_char[2] = 0x09;
			index_char[3] = 0x19;
			index_char[4] = 0x66;
			index_char[5] = 0x00;
			break;
		
		case 'S':
			index_char[0] = 0x46;
			index_char[1] = 0x49;
			index_char[2] = 0x49;
			index_char[3] = 0x49;
			index_char[4] = 0x31;
			index_char[5] = 0x00;
			break;
		
		case 'T':
			index_char[0] = 0x01;
			index_char[1] = 0x01;
			index_char[2] = 0x7F;
			index_char[3] = 0x01;
			index_char[4] = 0x01;
			index_char[5] = 0x00;
			break;
		
		case 'U':
			index_char[0] = 0x3F;
			index_char[1] = 0x40;
			index_char[2] = 0x40;
			index_char[3] = 0x40;
			index_char[4] = 0x3F;
			index_char[5] = 0x00;
			break;
		
		case 'V':
			index_char[0] = 0x1F;
			index_char[1] = 0x20;
			index_char[2] = 0x40;
			index_char[3] = 0x20;
			index_char[4] = 0x1F;
			index_char[5] = 0x00;
			break;
		
		case 'W':
			index_char[0] = 0x3F;
			index_char[1] = 0x40;
			index_char[2] = 0x78;
			index_char[3] = 0x40;
			index_char[4] = 0x3F;
			index_char[5] = 0x00;
			break;
		
		case 'X':
			index_char[0] = 0x63;
			index_char[1] = 0x14;
			index_char[2] = 0x08;
			index_char[3] = 0x14;
			index_char[4] = 0x63;
			index_char[5] = 0x00;
			break;
		
		case 'Y':
			index_char[0] = 0x03;
			index_char[1] = 0x0C;
			index_char[2] = 0x70;
			index_char[3] = 0x0C;
			index_char[4] = 0x03;
			index_char[5] = 0x00;
			break;
		
		case 'Z':
			index_char[0] = 0x61;
			index_char[1] = 0x51;
			index_char[2] = 0x49;
			index_char[3] = 0x45;
			index_char[4] = 0x43;
			index_char[5] = 0x00;
			break;
		
		case '0':
			index_char[0] = 0x7F;
			index_char[1] = 0x41;
			index_char[2] = 0x5D;
			index_char[3] = 0x41;
			index_char[4] = 0x7F;
			index_char[5] = 0x00;
			break;
		
		case '1':
			index_char[0] = 0x41;
			index_char[1] = 0x41;
			index_char[2] = 0x7F;
			index_char[3] = 0x40;
			index_char[4] = 0x40;
			index_char[5] = 0x00;
			break;
		
		case '2':
			index_char[0] = 0x31;
			index_char[1] = 0x49;
			index_char[2] = 0x49;
			index_char[3] = 0x49;
			index_char[4] = 0x46;
			index_char[5] = 0x00;
			break;
		
		case '3':
			index_char[0] = 0x41;
			index_char[1] = 0x49;
			index_char[2] = 0x49;
			index_char[3] = 0x49;
			index_char[4] = 0x36;
			index_char[5] = 0x00;
			break;
			
		case '4':
			index_char[0] = 0x0F;
			index_char[1] = 0x08;
			index_char[2] = 0x08;
			index_char[3] = 0x7F;
			index_char[4] = 0x08;
			index_char[5] = 0x00;
			break;
		
			case '5':
			index_char[0] = 0x4F;
			index_char[1] = 0x49;
			index_char[2] = 0x49;
			index_char[3] = 0x49;
			index_char[4] = 0x31;
			index_char[5] = 0x00;
			break;
			
		case '6':
			index_char[0] = 0x3E;
			index_char[1] = 0x49;
			index_char[2] = 0x49;
			index_char[3] = 0x49;
			index_char[4] = 0x31;
			index_char[5] = 0x00;
			break;
		
		case '7':
			index_char[0] = 0x01;
			index_char[1] = 0x01;
			index_char[2] = 0x01;
			index_char[3] = 0x01;
			index_char[4] = 0x7F;
			index_char[5] = 0x00;
			break;
		
		case '8':
			index_char[0] = 0x36;
			index_char[1] = 0x49;
			index_char[2] = 0x49;
			index_char[3] = 0x49;
			index_char[4] = 0x36;
			index_char[5] = 0x00;
			break;
		
		case '9':
			index_char[0] = 0x06;
			index_char[1] = 0x09;
			index_char[2] = 0x09;
			index_char[3] = 0x09;
			index_char[4] = 0x7E;
			index_char[5] = 0x00;
			break;
		
		case '.':
			index_char[0] = 0x60;
			index_char[1] = 0x60;
			index_char[2] = 0x00;
			index_char[3] = 0x00;
			index_char[4] = 0x00;
			index_char[5] = 0x00;
			break;
			
		case ':':
			index_char[0] = 0x00;
			index_char[1] = 0x00;
			index_char[2] = 0x36;
			index_char[3] = 0x00;
			index_char[4] = 0x00;
			index_char[5] = 0x00;
			break;
			
		case '/':
			index_char[0] = 0x60;
			index_char[1] = 0x10;
			index_char[2] = 0x08;
			index_char[3] = 0x04;
			index_char[4] = 0x03;
			index_char[5] = 0x00;
			break;
			
		case ' ':
			index_char[0] = 0x00;
			index_char[1] = 0x00;
			index_char[2] = 0x00;
			index_char[3] = 0x00;
			index_char[4] = 0x00;
			index_char[5] = 0x00;
			break;
		
		case ')':
			index_char[0] = 0x41;
			index_char[1] = 0x22;
			index_char[2] = 0x1C;
			index_char[3] = 0x00;
			index_char[4] = 0x00;
			index_char[5] = 0x00;
			break;
			
		//If a character is not recognized
		default:
			index_char[0] = 0x00;
			index_char[1] = 0x3E;
			index_char[2] = 0x3E;
			index_char[3] = 0x3E;
			index_char[4] = 0x00;
			index_char[5] = 0x00;
			break;
	}
	
	//set string to index_char so that it may be modified from calling function
	strncpy(data_string, index_char, 6);
}

uint8_t Display_Menu()
{
	uint8_t success_of 	= 1;
	uint8_t result 			= 0;
	
	char * SolisQ 			= "SOLISQ\0";
	char * Insta_RO 		= "0) INSTANT READOUT\0";
	char * CrossSec_RO 	= "1) X SECTION READOUT\0";
	char * Settings_RO 	= "2) SETTINGS\0";
	
	//2D array to display 6bit chars on 4 pages 
	char page[4][128];
	memset(page, 0, (sizeof(char) * 3 * 128));
	
	//Wipe the LCD clear
	LCD_Clean_Buffer();
	
	//Convert string to LCD values
	ConvertStringToLCD(SolisQ, page[0]);
	ConvertStringToLCD(Insta_RO, page[1]);
	ConvertStringToLCD(CrossSec_RO, page[2]);
	ConvertStringToLCD(Settings_RO, page[3]);
	
	
	//Send SolisQ string
	result = LCD_Write_Data(page[0], (strlen(SolisQ) * 6), 0x10, 0x01, 0xB4);
	
	//If transmission worked
	if (result != 1)
		success_of = 0;
	
	//Send Insta string
	result = LCD_Write_Data(page[1], (strlen(Insta_RO) * 6), 0x10, 0x01, 0xB5);
	
	//If transmission worked
	if (result != 1)
		success_of = 0;
	
	//Send CrossSec string
	result = LCD_Write_Data(page[2], (strlen(CrossSec_RO) * 6), 0x10, 0x01, 0xB6);
	
	//If transmission worked
	if (result != 1)
		success_of = 0;
	
	//Send Setting string
	result = LCD_Write_Data(page[3], (strlen(Settings_RO) * 6), 0x10, 0x01, 0xB7);
	
	//If transmission worked
	if (result != 1)
		success_of = 0;
	
	return success_of;
}

//This function will display the current readout of the velocity meter
uint8_t Display_Velocity()
{
	uint8_t sampling											= 1;
	uint8_t success_of										= 1;
	uint8_t result						 						= 0;
	uint8_t avg_index											= 0;
	uint8_t count 												= 0;
	char * symbols 												= "M/S\0";
	char * velocity 											= "VELOCITY\0";
	char * exit														= "B0 TO EXIT\0";
	uint32_t timer_1, timer_2, timer_diff	= 0;
	uint32_t timer_dsp										= 0;
	uint32_t timer_avg[5]								= {'\0'};
	char VV_buffer[7]											= {'\0'};
	
	//2D array to display 6bit chars on 4 pages 
	char page[4][128];
	memset(page, 0, (sizeof(char) * 4 * 128));
	
	//Wipe the screen
	LCD_Clean_Buffer();
	
	//Convert string to LCD values, then send to LCD
	ConvertStringToLCD(velocity, page[0]);
	result = LCD_Write_Data(page[0], (strlen(velocity) * 6), 0x10, 0x01, 0xB4);
	
	//If transmission did not work
	if (result != 1)
		success_of = 0;
	
	//Convert string to LCD values, then send to LCD
	ConvertStringToLCD(symbols, page[2]);
	result = LCD_Write_Data(page[2], (strlen(symbols) * 6), 0x10, 0x01, 0xB6);
	
	//If transmission did not work
	if (result != 1)
		success_of = 0;
	
	//Convert string to LCD values, then send to LCD
	ConvertStringToLCD(exit, page[3]);
	result = LCD_Write_Data(page[3], (strlen(exit) * 6), 0x10, 0x01, 0xB3);
	
	//If transmission did not work
	if (result != 1)
		success_of = 0;
	
	//Make sure the propellor pin in reading high (not starting on low, which means trigger)
	Debounce(PROPELLOR_INPUT_GPIO_Port, PROPELLOR_INPUT_Pin);
	
	//Make sure button 0 isnt pressed
	while(HAL_GPIO_ReadPin(BUTTON_0_GPIO_Port, BUTTON_0_Pin) == GPIO_PIN_RESET);
	
	//Start time check - initialize
	timer_1 = HAL_GetTick();
	
	do
	{		
		//If propellor pin sets low (trigger)
		if (HAL_GPIO_ReadPin(PROPELLOR_INPUT_GPIO_Port, PROPELLOR_INPUT_Pin) == GPIO_PIN_RESET)
		{
			//Get time, find differce, multiple by 2 since timer ticks are triggered every other MS tick
			timer_2 = HAL_GetTick();
			timer_diff = timer_2 - timer_1;
			timer_diff = timer_diff << 1;
			
			//Add timer value to buffer
			timer_avg[avg_index] = timer_diff;
			
			//Add all timer values together
			for (count = 0; count < 5; count++)
				timer_diff += timer_avg[count];
			
			//If we have sampled 5, average them 
			if (avg_index > 4)
			{
				avg_index = 0;
				timer_dsp = timer_diff / 5;
			}
			
			//Else keep gathering
			else
				avg_index++;			
			
			//Convert timer value to an LCD screen string, write to LCD
			CharToLCDValue(VV_buffer, 6, timer_dsp);
			ConvertStringToLCD(VV_buffer, page[1]);
			result = LCD_Write_Data(page[1], (strlen(VV_buffer) * 6), 0x10, 0x01, 0xB5);
			
			//If transmission did not work
			if (result != 1)
				success_of = 0;
						
			//Wait for signal to reset
			while (Debounce(PROPELLOR_INPUT_GPIO_Port, PROPELLOR_INPUT_Pin));
			
			//Start timer check
			timer_1 = HAL_GetTick();
		}	
		
		//Check to see if exit button is pressed
		if (Debounce(BUTTON_0_GPIO_Port, BUTTON_0_Pin))
				sampling = 0;
		
	} while(sampling);
	
	return success_of;
}

//This function takes in a hex character, then returns the ascii equivelent
char ConvertHexToChar(char hex_char)
{
	if (hex_char > 0x09)
		hex_char = hex_char + 55;
	
	else
		hex_char = hex_char + 48;
	
	return hex_char;
}

//Tihs funtion takes a uint32 value and transfers it to a character buffer for later conversion
void CharToLCDValue(char array[], uint8_t buffer_size, uint32_t value)
{
	uint8_t count = 0;
	
	//This for loop iterates through the array, starting at the top, and converts the values of hex to their ascii representation
	for (count = buffer_size; count != 0xFF; count--)
	{
		array[count] = (char)(value >> ((buffer_size - count) * 4) & 0x0F);
		array[count] = ConvertHexToChar(array[count]);
	}
}

//This function takes a string of data and formats it for the LCD
void ConvertStringToLCD(char data_string[], char array[])
{
	uint8_t count, index						= 0;
	char character_return[6]				= {'\0'};
	
	//Convert the data for the lcd
	for (count = 0; count < strlen(data_string); count++)
	{
		CHAR_Fetch(data_string[count], character_return);
		
		for (index = (count * 6); index < ((count + 1) * 6); index++)
			array[index] = character_return[(index - (count * 6))];		
	}
}

//This function performs a simple debounce test on a pin
uint8_t Debounce(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin)
{
	uint8_t debouncing 		= 1;
	uint8_t button_press 	= 0;
	
	while (debouncing)
	{
		//Detect initial press
		if(HAL_GPIO_ReadPin(GPIOx, GPIO_Pin) == GPIO_PIN_RESET)
		{
			HAL_Delay(5);
			
			//See if button is still pressed
			if(HAL_GPIO_ReadPin(GPIOx, GPIO_Pin) == GPIO_PIN_RESET)
			{
				debouncing 		= 0;
				button_press 	= 1;
			}
		}
		
		else
			debouncing = 0;
	}
		
	return button_press;
}

//This function fetches a button press and returns it. A timer value in MS can be sent
uint8_t ButtonPress(uint8_t s_timer)
{
	uint8_t button_press 	= 0xFF;
	uint16_t counter 			= 0;
	
	//If ms_timer is 0, pol buttons until a press is received
	if (!s_timer)
	{
		//Wait for a button press to be returned
			do
			{
				button_press = CheckButtons();
			}while(button_press == 0xFF);
	}
	
	else
	{
		//Until the button press or until the timer runs out, resolution is 1/5th of a second
		for (counter = 0; (counter < (s_timer * 5)) && (button_press != 0xFF); counter++)
		{
			button_press = CheckButtons();
			HAL_Delay(200);
		}		
	}
	
	return button_press;
}

//This function iwll poll each button
uint8_t CheckButtons()
{
	//FF is the default no button pressed selection
	uint8_t button_return = 0xFF;
	
	//Check each pin is pressed
	if (Debounce(BUTTON_0_GPIO_Port, BUTTON_0_Pin))
		button_return = 0;
	
	else if (Debounce(BUTTON_1_GPIO_Port, BUTTON_1_Pin))
		button_return = 1;
	
	else if (Debounce(BUTTON_2_GPIO_Port, BUTTON_2_Pin))
		button_return = 2;
	
	else if (Debounce(BUTTON_3_GPIO_Port, BUTTON_3_Pin))
		button_return = 3;
	
	else if (Debounce(BUTTON_4_GPIO_Port, BUTTON_4_Pin))
		button_return = 4;
	
	else if (Debounce(BUTTON_5_GPIO_Port, BUTTON_5_Pin))
		button_return = 5;
	
	return button_return;
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  file: The file name as string.
  * @param  line: The line in file as a number.
  * @retval None
  */
void _Error_Handler(char *file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
